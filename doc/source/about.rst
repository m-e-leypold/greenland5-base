About
=====

``greenland5-base`` provides foundational features that need to be
available to every Greenland5 package and a command line interface to
support generic operations on all installed Greenland5 packes.

At the moment this is the possibility to execute the built-in self
tests on all or only selected packages via ``greenland bist``.




